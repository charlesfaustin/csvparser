"""Unit tests."""
from parse_csv import parse_csv


def test_sanity():
    """Sanity test."""
    assert 1 == 1


def test_csv_3_output():
    """Test 3.csv."""
    expected_output = [{'day': 'mon', 'description': 'third_desc 9', 'square': 9, 'value': 3},
                       {'day': 'tue', 'description': 'third_desc 9', 'square': 9, 'value': 3},
                       {'day': 'wed', 'description': 'third_desc 4', 'square': 4, 'value': 2},
                       {'day': 'thu', 'description': 'third_desc 4', 'double': 4, 'value': 2},
                       {'day': 'fri', 'description': 'third_desc 2', 'double': 2, 'value': 1}]
    assert parse_csv('3.csv') == expected_output


def test_csv_2_output():
    """Test 2.csv."""
    expected_output = [{'day': 'mon', 'description': 'second_desc 4', 'square': 4, 'value': 2},
                       {'day': 'tue', 'description': 'second_desc 4', 'square': 4, 'value': 2},
                       {'day': 'wed', 'description': 'second_desc 4', 'square': 4, 'value': 2},
                       {'day': 'thu', 'description': 'second_desc 4', 'double': 4, 'value': 2},
                       {'day': 'fri', 'description': 'second_desc 6', 'double': 6, 'value': 3}]
    assert parse_csv('2.csv') == expected_output


def test_csv_1_output():
    """Test 1.csv."""
    expected_output = [{'day': 'mon', 'description': 'first_desc 1', 'square': 1, 'value': 1},
                       {'day': 'tue', 'description': 'first_desc 25', 'square': 25, 'value': 5},
                       {'day': 'wed', 'description': 'first_desc 4', 'square': 4, 'value': 2},
                       {'day': 'thu', 'description': 'first_desc 6', 'double': 6, 'value': 3},
                       {'day': 'fri', 'description': 'first_desc 6', 'double': 6, 'value': 3}]
    assert parse_csv('1.csv') == expected_output
