"""Parse csv module."""
import sys

# Days allowed in csv.
fixed_days = ['mon', 'tue', 'wed', 'thu', 'fri']


square_func = ['square', lambda x: int(x)**2]
double_func = ['double', lambda x: int(x)*2]


sq_or_dbl = {'mon':square_func,
             'tue':square_func,
             'wed':square_func,
             'thu':double_func,
             'fri':double_func}


def run_day(dayitem):
    if dayitem in fixed_days:
        return dayitem
    if len(dayitem.split('-')) == 2:
        two_items = dayitem.split('-')
        first_point = fixed_days.index(two_items[0])
        second_point = fixed_days.index(two_items[1])
        return ','.join(fixed_days[first_point:second_point+1])

def parse_csv(filename):
    with open('csv_files/{}'.format(filename)) as f:
        text_lines = f.readlines()
        dates = text_lines[0].split(',')
        second_line = text_lines[1].split(',')    

        some_dict = {'days':{}}
        for index , obj in enumerate(dates):
            if obj.strip() == 'description':
                some_dict['description'] = second_line[index].replace('\n', '')
            if run_day(obj) is not None:
                some_dict['days'][index] = run_day(obj)    

        final_dict_list = [] 
        for uu in some_dict['days'].items():
            for tt in uu[1].split(','):
                value_num = second_line[uu[0]]
                square = int(second_line[uu[0]]) ** 2
                final_dict_list.append({'day':tt,
                       'description':'{} {}'.format(some_dict['description'], sq_or_dbl[tt][1](value_num)),
                       sq_or_dbl[tt][0]: sq_or_dbl[tt][1](value_num),
                       'value': int(value_num)})
        return final_dict_list


if __name__ == '__main__':
    returned_dict = parse_csv(sys.argv[1])
    print(returned_dict)
