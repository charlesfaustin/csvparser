The Commands
--------------

### Setup virtualenv

-   Make sure you use python 3.6.4:

        $ virtualenv -p python3 parser
        $ . /path/to/virtualenv/bin/activate


### Install dependencies

-   use pip:
        $ pip install -r requirements.txt


### Run tests from root directory

-   Make sure you are in root directory:
        $ py.test


### Run Script from root directory

-   Make sure you are in root directory, & file name exists inside csv_files folder:
        $ python3 parse_csv.py 1.csv
        $ python3 parse_csv.py 2.csv
        $ python3 parse_csv.py 3.csv


